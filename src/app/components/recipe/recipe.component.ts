import {Component, Input} from '@angular/core';
import {Recipe} from "../../models/recipe.model";
import {MatButtonModule} from '@angular/material/button';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})

export class RecipeComponent {

constructor(){
}

@Input() recipe!:Recipe;

showIngredients:boolean = false;


public toggleIngredients():void{
  this.showIngredients=!this.showIngredients;
}

}
