import { Recipe } from 'src/app/models/recipe.model';
import { Component, OnInit } from '@angular/core';
import { RecipeService } from 'src/app/services/recipe/recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

  public recipeList?: Recipe[];

  constructor(private recipeService: RecipeService){ }

  public ngOnInit(): void {
    this.getRecipeList();
  }

public getRecipeList(): void {
  this.recipeService.getRecipes().subscribe({
    next: (result:Recipe[]) => {this.recipeList=result;}
  });
}

}
