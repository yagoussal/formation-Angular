import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeByIdComponent } from './recipe-by-id.component';

describe('RecipeByIdComponent', () => {
  let component: RecipeByIdComponent;
  let fixture: ComponentFixture<RecipeByIdComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RecipeByIdComponent]
    });
    fixture = TestBed.createComponent(RecipeByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
