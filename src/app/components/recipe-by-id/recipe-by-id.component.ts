import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe/recipe.service';

@Component({
  selector: 'app-recipe-by-id',
  templateUrl: './recipe-by-id.component.html',
  styleUrls: ['./recipe-by-id.component.scss']
})
export class RecipeByIdComponent {

  public recipe?: Recipe;
  private id!: string;

  constructor(private recipeService: RecipeService, private route:ActivatedRoute){ }

  public ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id') || '';
    this.getRecipeById();
  }

public getRecipeById(): void {
  this.recipeService.getRecipeById(this.route.snapshot.paramMap.get('id') || '').subscribe({
    next: (result:Recipe) => {this.recipe=result;}
  });
}

}
