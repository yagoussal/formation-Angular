import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';

import { Recipe } from 'src/app/models/recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {


  private recipeListUrl = 'http://52.47.189.244:8080/api/v1/recipes';
  
  public getRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>(this.recipeListUrl);
  }


  private recipeByIdUrl = 'http://52.47.189.244:8080/api/v1/recipes/';
  
  public getRecipeById(id: string): Observable<Recipe> {
    return this.http.get<Recipe>(this.recipeByIdUrl + id);
  }



  constructor(private http: HttpClient) { }

}
