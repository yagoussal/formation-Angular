import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { RecipeByIdComponent } from './components/recipe-by-id/recipe-by-id.component';

const routes: Routes = [
  {
    path:'recipes/:id',
    component: RecipeByIdComponent,
    pathMatch:'full'
  },
  {
    path:'recipes',
    component: RecipeListComponent,
    pathMatch:'full'
  },
  {
    path:'**',
    redirectTo: 'recipes',
    pathMatch:'full'
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
